﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    //Variables
    public float Time;

    // Start is called before the first frame update
    void Start()
    {
        //Destroy the object after the time input
        Destroy(this.gameObject, Time);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
