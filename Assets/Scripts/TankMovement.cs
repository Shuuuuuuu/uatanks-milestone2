﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovement : MonoBehaviour
{
    public int players = 1;          //Tanks that belong to which player.
    public float moveSpeed = 12f;    //Tank speed
    public float turnSpeed = 180f;   //Tank turn
    public AudioSource moveAudio;    //Audio when tank is moving
    public AudioClip EngineIdling;   //Audio when tank is doing nothing
    public AudioClip EngineDriving;  //Audio when tank si driving
    public float moveRange = 0.2f;   //The amount by which the engine noises varies.

    private string movementAxis;     //Name of the axis use for movement
    private string turnAxis;         //Name of axis for turning
    private Rigidbody rb;            //Reference the rigidbody from the tank
    private float movementInput;     //Value for movement
    private float turnInput;         //Value for turn
    private float pitch;             //The pitch of the audi source at the start

    void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();  //GetComponent From Rigidbody of Prefab
    }

    void Start()
    {
        //The axes name based on the player
        movementAxis = "Vertical" + players;
        turnAxis = "Horizontal" + players;

        //Original pitch for the audio source
        pitch = moveAudio.pitch;
    }

    
    void Update()
    {
        //Value of both input axes
        movementInput = Input.GetAxisRaw(movementAxis);
        turnInput = Input.GetAxisRaw(turnAxis);
        EngineAudio();
    }

    void OnEnable()
    {
        //Tank is turned on and it is not kinematic
        rb.isKinematic = false;

        //Reset the input values
        movementInput = 0f;
        turnInput = 0f;
    }

    void OnDisable()
    {
        //Tank is turned off kinematic its on making it stop moving
        rb.isKinematic = true;
    }

    void EngineAudio()
    {
        //if there is no input the tank stays in its place
        if(Mathf.Abs (movementInput) < 0.1f && Mathf.Abs (turnInput) < 0.1f)
        {
            //If the audio source is currently playing the driving clip change to idle
            if(moveAudio.clip == EngineDriving)
            {
                moveAudio.clip = EngineIdling;
                moveAudio.pitch = Random.Range(pitch - moveRange, pitch + moveRange);
                moveAudio.Play();
            }
        }

        else
        {
            //When tank moves changes from idling to driving
            if(moveAudio.clip == EngineIdling)
            {
                moveAudio.clip = EngineDriving;
                moveAudio.pitch = Random.Range(pitch - moveRange, pitch + moveRange);
                moveAudio.Play();
            }
        }
    }

    void FixedUpdate()
    {
        //Adjust the rigidbodies position and rotation
        Move();
        Turn();
    }

    void Move()
    {
        //Where the tank will be facing frame by frame
        Vector3 movement = transform.forward * movementInput * moveSpeed * Time.deltaTime;
        rb.MovePosition(rb.position + movement);
    }

    void Turn()
    {
        //the number of degress to turn on the input using speed and time
        float turn = turnInput * turnSpeed * Time.deltaTime;

        //Rotation in y axis
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

        //Rotation applied to rigidbody's rotation
        rb.MoveRotation(rb.rotation * turnRotation);
    }

    //When get the Speed Item receive speed
    float addSpeed()
    {
        moveSpeed += 5;
        Invoke("ReduceSpeed", 10);
        return moveSpeed;
    }

    //Return to normal after 10 seconds
    float ReduceSpeed()
    {
        moveSpeed -= 5;
        return moveSpeed;
    }
}
