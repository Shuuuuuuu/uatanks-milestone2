﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    //Variables
    public int targetCount = 5;
    public float radius = 10;
    public GameObject enemyPrefab;

    //Call In An IEnumerator to Spawn Tanks and How Much to Wait And Random Position
    System.Collections.IEnumerator SpawnTanks()
    {
        yield return new WaitForSeconds(3);
        while (true)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            if(enemies.Length < targetCount)
            {
                GameObject enemy = GameObject.Instantiate(enemyPrefab);
                Vector2 c = Random.insideUnitCircle * radius;
                enemy.transform.position = new Vector3(c.x, 0, c.y);
            }
            yield return new WaitForSeconds(2);
        }
    }

    void Start()
    {
        //Start Spawning the enemy
        StartCoroutine(SpawnTanks());
    }
}
