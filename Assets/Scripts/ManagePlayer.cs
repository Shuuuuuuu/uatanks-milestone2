﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagePlayer : MonoBehaviour
{
    //Variable
    public GameObject tankPlayer;

    // Start is called before the first frame update
    void Start()
    {
        //Initiate the tankPlayer
        Instantiate(tankPlayer);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
